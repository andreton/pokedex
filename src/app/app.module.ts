import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonListComponent } from './Components/pokemon-list/pokemon-list.component';
import { HttpClientModule } from '@angular/common/http';
import { TrainerPageComponent } from './Components/trainer-page/trainer-page.component';
import { FormsModule } from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { LandingPageComponent } from './Components/landing-page/landing-page.component';
import { HeaderComponent } from './Components/header/header.component';
import { FooterComponent } from './Components/footer/footer.component';
import { PokemondetailComponent } from './Components/pokemondetail/pokemondetail.component'
import { MaterialModule } from 'src/shared/material.module';



@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    TrainerPageComponent,
    LandingPageComponent,
    HeaderComponent,
    FooterComponent,
    PokemondetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatGridListModule,
    MaterialModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
