import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
 
  register(trainerName){
      localStorage.setItem('trainerName',JSON.stringify(trainerName));
  }
  /* register(trainer):Promise<any>{
    return this.http.post('',{
      trainer:{...trainer}
    }).toPromise();
  }*/
 
}
