import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import { ValueTransformer } from '@angular/compiler/src/util';


@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  constructor(private http: HttpClient) 
  { }

  getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, avatar: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }

  getPokemons(){
    return this.http.get<any>(`${environment.apiURL}?limit=50`).pipe(
      map(response=>{
        return response.results.map(pokemon=>{
          return {
                  ...this.getIdAndImage(pokemon.url),
                  ...pokemon
          }
        })
      })
    )
  }
  
  getPokemonById(index){
    return this.http.get<any>(`${environment.apiURL}${index}`);
  }



   

  
}
        