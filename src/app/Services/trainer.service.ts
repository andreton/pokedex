import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  arrayOfCatchedPokemonsId:any=[];
  arrayOfCatchedPokemonsName:any=[];
  

  constructor() { }
  
  getTrainerName(){
    return localStorage.getItem("trainerName");
  }

}
