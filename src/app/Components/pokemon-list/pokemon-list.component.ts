import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/Services/pokemon.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list'
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  pokemonId: string;

  image: string = '';
  allPokemons = [];
  pokemonData: any[] = [];
  isLoading: boolean = false;
  registerError: string = "";


  constructor(private pokemonService: PokemonService,private router:Router) { }

  ngOnInit(): void {
    this.getPokemons();
  }
  getPokemons() {
    this.registerError = '';
    this.pokemonService.getPokemons().subscribe(
      pokemons => {
        this.pokemonData = [...pokemons]
      },
      err => {
        console.log(err);
      }
    );
  }
  getPokemonId(pokemon){
    this.router.navigateByUrl(`pokemonDetails/${pokemon.id}`);
  }
}
