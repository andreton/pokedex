import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/Services/pokemon.service';
import { TrainerService } from 'src/app/Services/trainer.service';
@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.scss']
})
export class TrainerPageComponent implements OnInit {
  trainer={
    name:''
  }
  pokemonId:string;
  pokemonUrl:any[]=[];
  pokemonName:any[]=[];
  

  constructor(private trainerService:TrainerService, private router:Router,private pokemonService:PokemonService) { }

  ngOnInit(): void {
    this.getCurrentTrainer();
    this.pokemonId=this.trainerService.arrayOfCatchedPokemonsId;
    this.pokemonName=this.trainerService.arrayOfCatchedPokemonsName;

  }

  async getCurrentTrainer(){
    try{
      this.trainer.name=this.trainerService.getTrainerName();
    }
    catch(e){
      console.log(e)
    }
  }
 
  goToPokemonList(){  
    this.router.navigateByUrl('/pokemons');
  }

  

  
}

