import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  trainer={
    name:""
  }
  isLoading:boolean=false;
  logInError:string="";

  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.checkIfTrainerExists();
  }

  async onRegisterClicked(){
    try{
      this.isLoading=true;
      const result:any=await this.auth.register(this.trainer.name);
    }
    catch(e){
      console.error(e);
    }
    finally{
      this.isLoading=false;
      this.router.navigateByUrl('/trainer')
    }
  }
   checkIfTrainerExists(){
     //Method for getting key values of localstorage
      var values = [],
          keys = Object.keys(localStorage),
          i = keys.length;
      while ( i-- ) {
          values.push( localStorage.getItem(keys[i]) );
      }
      if (localStorage.getItem("trainerName") == null) {
          return;
      }
      else{
        this.router.navigateByUrl('/trainer');
      }
  
  
  }

}
