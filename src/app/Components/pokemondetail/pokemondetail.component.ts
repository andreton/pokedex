import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from 'src/app/Services/pokemon.service';
import { TrainerService } from 'src/app/Services/trainer.service';

@Component({
  selector: 'app-pokemondetail',
  templateUrl: './pokemondetail.component.html',
  styleUrls: ['./pokemondetail.component.scss']
})
export class PokemondetailComponent implements OnInit {
  pokemon: any='';
  pokemonImage:string='';

  constructor(private pokemonService:PokemonService, private activatedRouter:ActivatedRoute, private router:Router,private trainerService:TrainerService ) {
    //Getting the passed id from the pokemon list component
    this.activatedRouter.params.subscribe(
      params=>{
        this.getPokemonById(params['id']);
      }
    )
  }

  ngOnInit(): void {     
  }
  catchPokemon(){ 
      this.trainerService.arrayOfCatchedPokemonsId.push(this.pokemon.id); 
      this.trainerService.arrayOfCatchedPokemonsName.push(this.pokemon.id);
      console.log(this.trainerService.arrayOfCatchedPokemonsId);
       this.router.navigateByUrl('/trainer');  }

  getPokemonById(id){
    this.pokemonService.getPokemonById(id).subscribe(
      res=>{
        this.pokemon=res;
        this.pokemonImage=res.sprites.front_default;
        console.log(this.pokemonImage);
        console.log(res);
      },
      err=>{
        console.log(err);
      }
    )
  }

}
