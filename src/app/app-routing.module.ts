import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './Components/landing-page/landing-page.component';
import { PokemonListComponent } from './Components/pokemon-list/pokemon-list.component';
import { PokemondetailComponent } from './Components/pokemondetail/pokemondetail.component';
import { TrainerPageComponent } from './Components/trainer-page/trainer-page.component';

const routes: Routes = [
  {
    path: "pokemons",
    component: PokemonListComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landingpage'
  },
  {
    path:'trainer',
    component:TrainerPageComponent
  },
  {
    path: 'landingpage',
    component:LandingPageComponent
  },
  {
    path: 'pokemonDetails/:id',
    component: PokemondetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
